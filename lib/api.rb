require 'net/http'
require 'uri'
require 'json'

# send and get POST to API
class Api
  attr_accessor :url, :api_key
  def get_task
    tmp = send(key: api_key, method: 'GetTasks', params: nil)
    JSON.parse(tmp.body)
  end

  def check_results(answers)
    user = { key: api_key, method: 'CheckResults', params: [] }
    answers.each do |d|
      user[:params] << { left_bottom: { x: d[0], y: d[1] }, right_top: { x: d[2], y: d[3] } }
    end
    JSON.parse(send(user).body)
  end

  private

  def send(request_body)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    header = { 'content-type' => 'application/json' }
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = request_body.to_json
    http.request(request)
  end
end
