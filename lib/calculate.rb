# implemented some alghoritms
class Calculate
  def find_extreme_point(tasks)
    answers = []
    tasks['result'].each do |task|
      x_values = []
      y_values = []
      task.each do |data|
        x_values += push_point_to_array(data['x'], data['radius'])
        y_values += push_point_to_array(data['y'], data['radius'])
      end
      answers.push([x_values.min, y_values.min, x_values.max, y_values.max])
    end
    answers
  end

  private

  def push_point_to_array(var, radius)
    [(var + radius), (var - radius)]
  end
end
