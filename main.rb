#!/usr/bin/ruby
require_relative 'lib/api'
require_relative 'lib/calculate'

puts '$./main.rb --api_key=input_your_api_key_here' if ARGV.empty?

args = {}
ARGV.each do |arg|
  match = /--(?<key>.*?)=(?<value>.*)/.match(arg)
  args[match[:key]] = match[:value]
end

exit if args['api_key'].nil?

a = Api.new
a.url = args['url'] || 'http://93.91.165.233:8081/api'
a.api_key = args['api_key']
tasks = a.get_task
b = Calculate.new
extreme_point = b.find_extreme_point(tasks)
finish = a.check_results(extreme_point)
p finish
